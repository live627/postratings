<?php

/**********************************************************************************
* POST RATINGS PRO 1.5															  *
***********************************************************************************
* Created by:            Solomon Closson (aka: SoLoGHoST)     					  *
* Copyright 2014:     Solomon Closson (http://dream-portal.net)					  *
* =============================================================================== *
* PostRatings.template.php 											              *
**********************************************************************************/

// Main template responsible for loading up Ratings on a message-by-message basis!
function template_main()
{
	global $context, $settings, $txt;
	
		echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"', $context['right_to_left'] ? ' dir="rtl"' : '', '>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=', $context['character_set'], '" />
		<meta name="robots" content="noindex" />
		<title>', $context['page_title'], '</title>
		<link rel="stylesheet" type="text/css" href="', $settings['theme_url'], '/css/index.css" />
		<link rel="stylesheet" type="text/css" href="', $settings['default_theme_url'], '/css/postratings.css?ver1.5" />
		<script type="text/javascript" src="', $settings['default_theme_url'], '/scripts/script.js"></script>
	</head>
	<body>
	<div class="description">';

	$count = count($context['pratings']);
	foreach($context['pratings'] as $k => $rater)
	{
		$windowbg = 'windowbg' . (($k+1)%2 == 0 ? '2' : '');
		echo '
			<div class="prWrapper ', $windowbg, (($k+1) == $count ? (($k == 0) ? ' first last' : ' last') : ($k == 0 ? ' first' : '')), '">
				<div class="floatright smalltext">', $rater['rating'], '<div class="clear">', $rater['date'], '</div></div>
				<div class="userWrapper">
					<div>', (!empty($rater['avatar_link']) ? $rater['avatar_link'] : $rater['member_link']), '</div>
				</div>
			</div>';
	}
	echo '
		<br /><a href="javascript:self.close();">', $txt['close_window'], '</a>
	</div>
	</body>
</html>';
}
?>