<?php
/*
Post Ratings Pro
Version 1.0
By: SoLoGHoST
http://dream-portal.net
Copyright 2013 Solomon Closson

############################################
License Information:

ABOVE INFO MUST REMAIN INTACT!!
#############################################
*/

// If SSI.php is in the same place as this file, and SMF isn't defined...
if (file_exists(dirname(__FILE__) . '/SSI.php') && !defined('SMF'))
  require_once(dirname(__FILE__) . '/SSI.php');
// Hmm... no SSI.php and no SMF?
elseif (!defined('SMF'))
  die('<b>Error:</b> Cannot install - please verify you put this in the same place as SMF\'s index.php.');

if((SMF == 'SSI') && !$user_info['is_admin']) 
    die('Admin priveleges required.');

db_extend('packages');

// the columns to add
$column_add = array();
$column_indexes = array();

$column_add[] = array(
	'table' => 'messages',
	'info' => array(
		'name' => 'rating',
		'type' => 'smallint',
		'size' => 4,
		'null' => false,
		'default' => 0,
		'unsigned' => true,
	),
);

$column_add[] = array(
	'table' => 'messages',
	'info' => array(
		'name' => 'total_ratings',
		'type' => 'int',
		'size' => 10,
		'null' => false,
		'default' => 0,
		'unsigned' => true,
	),
);

$column_add[] = array(
	'table' => 'messages',
	'info' => array(
		'name' => 'ratings_enabled',
		'type' => 'tinyint',
		'size' => 4,
		'null' => false,
		'default' => 0,
	),
);

$column_add[] = array(
	'table' => 'messages',
	'info' => array(
		'name' => 'id_last_rating',
		'type' => 'int',
		'size' => 10,
		'null' => false,
		'default' => 0,
		'unsigned' => true,
	),
);

$column_indexes[] = array(
	'table' => 'messages',
	'info' => array(
		'type' => 'unique',
		'columns' => array('id_last_rating', 'id_msg'),
	),
);

// add the columns into topics table
$column_add[] = array(
	'table' => 'topics',
	'info' => array(
		'name' => 'is_ratings',
		'type' => 'tinyint',
		'size' => 4,
		'null' => false,
		'default' => 0,
	),
);

$column_indexes[] = array(
	'table' => 'topics',
	'info' => array(
		'type' => 'key',
		'columns' => array('is_ratings'),
	),
);

// now for the message ratings table
$post_ratings_columns[] = array(
	'name' => 'id_rating',
	'type' => 'int',
	'size' => 11,
	'unsigned' => true,
	'null' => false,
	'auto' => true,
);
$post_ratings_columns[] = array(
	'name' => 'id_msg',
	'type' => 'int',
	'size' => 11,
	'null' => false,
	'default' => 0,
	'unsigned' => true,
);
$post_ratings_columns[] = array(
	'name' => 'id_member',
	'type' => 'mediumint',
	'size' => 8,
	'null' => false,
	'default' => 0,
	'unsigned' => true,
);

$post_ratings_columns[] = array(
	'name' => 'date',
	'type' => 'int',
	'size' => 10,
	'null' => false,
	'default' => 0,
	'unsigned' => true,
);
$post_ratings_columns[] = array(
	'name' => 'value',
	'type' => 'tinyint',
	'size' => 2,
	'null' => false,
);

// Indexes
$post_ratings_columns_indexes[] = array(
	'type' => 'primary',
	'columns' => array('id_rating')
);

$post_ratings_columns_indexes[] = array(
	'type' => 'key',
	'columns' => array('id_msg')
);

// Create the tables...
$smcFunc['db_create_table']('{db_prefix}message_ratings', $post_ratings_columns, $post_ratings_columns_indexes, array(), 'ignore');

// Insert the column(s) into the pm_recipients table
if (!empty($column_add))
	foreach ($column_add as $column)
		$smcFunc['db_add_column']('{db_prefix}' . $column['table'], $column['info'], array(), 'ignore');

// Add the indexes...
if (!empty($column_indexes))
	foreach ($column_indexes as $index)
		$smcFunc['db_add_index']('{db_prefix}' . $index['table'], $index['info'], array(), 'ignore');
		
// Insert the settings
$smcFunc['db_insert']('ignore',
            '{db_prefix}settings',
            array(
				'variable' => 'string', 'value' => 'string',
			),
            array(
				   // This setting directly below will need to change!
                   array('post_ratings_type', '0'),
				   array('post_ratings_first_post', '0'),
				   array('post_ratings_autoenable_topic_mode', '0'),
				   array('post_ratings_auto_expanded', '2'),
				   array('post_ratings_disable_locked_topic', '0'),
				   array('post_ratings_limit_count', '0'),
				   array('post_ratings_show_quantity', '1'),
				   array('post_ratings_show_lastratedtime', '1'),
				   array('post_ratings_posts_enable_icon', '1'),
				   array('post_ratings_posts_delete_icon', '1'), 
				   array('post_ratings_image_rated', 'Themes/default/images/post_ratings/starFull.png'),
				   array('post_ratings_image_nonrated', 'Themes/default/images/post_ratings/starEmpty.png'),
				   array('post_ratings_text1', 'Poor'),
				   array('post_ratings_text2', 'Ok'),
				   array('post_ratings_text3', 'Good'),
				   array('post_ratings_text4', 'Great'),
				   array('post_ratings_text5', 'Excellent'),
				   array('post_ratings_not_rated', '[b]Not Yet Rated![/b]'),
				   array('post_ratings_not_expanded_rate', '[b]Post Rating Options (Rate this post)[/b]'),
				   array('post_ratings_not_expanded_edit', 'Post Rating Options'),
				   array('post_ratings_not_rated_body', 'Rate this post:'),
				   array('post_ratings_rated_body', 'Your Rating:'),
				   array('post_ratings_th1', '20'),
				   array('post_ratings_th2', '50'),
				   array('post_ratings_th3', '75'),
				   array('post_ratings_th4', '90'),
				   array('post_ratings_level', '1'),
				   array('post_ratings_layout_style', '0'),
				   array('post_ratings_highestrated_title', 'Highest Rated {POST RATINGS}'),
				   array('post_ratings_userrated_title', 'Most Rated User(s)'),
            ),
			array('variable')
        );

?>
