<?php
/*
Post Ratings Pro
Version 1.0
By: SoLoGHoST
http://dream-portal.net
Copyright 2013 Solomon Closson

############################################
License Information:

ABOVE INFO MUST REMAIN INTACT!!
#############################################
*/

if (file_exists(dirname(__FILE__) . '/SSI.php'))
  require_once(dirname(__FILE__) . '/SSI.php');
// Hmm... no SSI.php and no SMF?
elseif (!defined('SMF'))
  die('<b>Error:</b> Cannot uninstall - please verify you put this in the same place as SMF\'s index.php.');

// Only Admin can uninstall...
if((SMF == 'SSI') && !$user_info['is_admin']) 
    die('Admin priveleges required.'); 

// Load the settings to be removed into an array ;)
$row_settings = array(
		'post_ratings_type',
		'post_ratings_boards',
		'post_ratings_first_post',
		'post_ratings_autoenable_topic_mode',
		'post_ratings_auto_expanded',
		'post_ratings_disable_locked_topic',
		'post_ratings_limit_count',
		'post_ratings_show_quantity',
		'post_ratings_show_lastratedtime',
		'post_ratings_posts_enable_icon',
		'post_ratings_posts_delete_icon',
		'post_ratings_image_nonrated',
		'post_ratings_image_rated',
		'post_ratings_text1',
		'post_ratings_text2',
		'post_ratings_text3',
		'post_ratings_text4',
		'post_ratings_text5',
		'post_ratings_not_rated',
		'post_ratings_not_expanded_rate',
		'post_ratings_not_expanded_edit',
		'post_ratings_not_rated_body',
		'post_ratings_rated_body',
		'post_ratings_th1',
		'post_ratings_th2',
		'post_ratings_th3',
		'post_ratings_th4',
		'post_ratings_level',
		'post_ratings_layout_style',
		'post_ratings_stats_locked_topics',
		'post_ratings_stats_ordering',
		'post_ratings_stats_highestusers_limit',
		'post_ratings_stats_highestrated_limit',
		'post_ratings_highestrated_title',
		'post_ratings_userrated_title',
		'post_ratings_stats_users_day',
	);

// UNINSTALLING Post Ratings...
db_extend('packages');

// Get rid of the message_ratings table.
$smcFunc['db_drop_table'] ('{db_prefix}message_ratings');

$db_remove = array('messages' => array('rating', 'total_ratings', 'ratings_enabled', 'id_last_rating'), 'topics' => 'is_ratings');

// Get rid of the added columns throughout all of the tables in SMF
foreach($db_remove as $table => $columns)
{
	if (is_array($columns))
		foreach($columns as $column)
			$smcFunc['db_remove_column']('{db_prefix}' . $table, $column);
	else
		$smcFunc['db_remove_column']('{db_prefix}' . $table, $columns);
}

// Let's remove the settings
if (!empty($row_settings))
	$smcFunc['db_query']('', '
	   DELETE FROM {db_prefix}settings
	   WHERE variable IN ({array_string:settings_list})',
	   array(
			'settings_list' => $row_settings
	   )
	);

?>