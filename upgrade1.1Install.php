<?php
/*
Post Ratings Pro
Version 1.1
By: SoLoGHoST
http://dream-portal.net
Copyright 2013 Solomon Closson

############################################
License Information:

ABOVE INFO MUST REMAIN INTACT!!
#############################################
Notes:  This file removes all "ratings_topic" Moderation logs from your log_actions table!
*/

// If SSI.php is in the same place as this file, and SMF isn't defined...
if (file_exists(dirname(__FILE__) . '/SSI.php') && !defined('SMF'))
  require_once(dirname(__FILE__) . '/SSI.php');
// Hmm... no SSI.php and no SMF?
elseif (!defined('SMF'))
  die('<b>Error:</b> Cannot install - please verify you put this in the same place as SMF\'s index.php.');

if((SMF == 'SSI') && !$user_info['is_admin']) 
    die('Admin priveleges required.');

db_extend('packages');

// Clear out all of the faulty Moderation Logs from Post Ratings Pro 1.0
$smcFunc['db_query']('', '
   DELETE FROM {db_prefix}log_actions
   WHERE action = {string:topic_ratings}',
   array(
		'topic_ratings' => 'ratings_topic',
   )
);

?>