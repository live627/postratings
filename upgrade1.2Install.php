<?php
/*
Post Ratings Pro
Version 1.2
By: SoLoGHoST
http://dream-portal.net
Copyright 2013 Solomon Closson
############################################
License Information:

ABOVE INFO MUST REMAIN INTACT!!
#############################################
File:	upgrade1.2Install.php
*/

// If SSI.php is in the same place as this file, and SMF isn't defined...
if (file_exists(dirname(__FILE__) . '/SSI.php') && !defined('SMF'))
  require_once(dirname(__FILE__) . '/SSI.php');
// Hmm... no SSI.php and no SMF?
elseif (!defined('SMF'))
  die('<b>Error:</b> Cannot install - please verify you put this in the same place as SMF\'s index.php.');

if((SMF == 'SSI') && !$user_info['is_admin']) 
    die('Admin priveleges required.');

db_extend('packages');


// Insert the new settings
$smcFunc['db_insert']('ignore',
	'{db_prefix}settings',
	array(
		'variable' => 'string', 'value' => 'string',
	),
	array(
		   array('post_ratings_not_rated_body', 'Rate this post:'),
		   array('post_ratings_rated_body', 'Your Rating:'),
		   array('post_ratings_highestrated_title', 'Highest Rated {POST RATINGS}'),
		   array('post_ratings_userrated_title', 'Most Rated User(s)'),
	),
	array('variable')
);



?>